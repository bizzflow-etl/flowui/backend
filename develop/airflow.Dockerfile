FROM registry.gitlab.com/bizzflow-etl/toolkit

ADD develop/airflow.entrypoint.sh .
ADD develop/distill-credentials.py .

ENTRYPOINT ["./airflow.entrypoint.sh"]
