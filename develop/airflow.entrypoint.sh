#!/usr/bin/env bash

set -a -e -o pipefail

airflow db init

airflow users create -r Admin -u bizzflow -f Bizzflow -l Administrator -p wolfzzib -e test@test.com

[ -f "/home/bizzflow/projects/project/project.json" ] || {
    echo "Project configuration was not found"
    curl -L https://gitlab.com/bizzflow-etl/base-project/-/archive/master/base-project-master.tar | tar -x -C /tmp/
    mkdir -p /home/bizzflow/projects/project/ || echo "Destination exists"
    mv /tmp/base-project-master/* /home/bizzflow/projects/project/
}

python3 distill-credentials.py

airflow webserver
