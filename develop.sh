#!/usr/bin/bash

set -e -o pipefail

export GID=$(id -g)

GID=$GID docker-compose $1 -f develop-compose.yml up
