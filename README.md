# Bizzflow UI / Backend

## Deployment

### Deploy to Bizzflow R2 (without Docker)

#### Install Docker

Old installations use CentOS, if your OS is different, follow official guide.

```console
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo


sudo yum -y install docker-ce docker-ce-cli containerd.io

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo systemctl enable docker
sudo systemctl start docker

sudo usermod -aG docker `whoami`
```

#### Edit `nginx` configuration

Open `/etc/nginx/nginx.conf` and add following two `location` clauses:

```nginx
server {
# ...

location /flow/ {
    proxy_pass http://127.0.0.1:9091/;
    proxy_set_header Host $host;
    proxy_set_header    X-Real-IP           $remote_addr;
    proxy_set_header    X-Forwarded-Host    localhost;
    proxy_set_header    X-Forwarded-Server  localhost;
    proxy_set_header    X-Forwarded-Proto   $scheme;
    proxy_set_header    X-Forwarded-For     $remote_addr;
    proxy_redirect off;
    proxy_http_version 1.1;
}

location /flow/api/ {
    proxy_pass http://127.0.0.1:9090/;
    proxy_set_header Host $host;
    proxy_redirect off;
    proxy_http_version 1.1;
}

# ...
}
```

Check `nginx` configuration validity (`sudo nginx -t`) and restart webserver:

```console
sudo systemctl restart nginx
```

#### Run

Create file `project.env` file to user's home dir (`/home/centos` by default) and put there:

```env
FLOW_UI_FRONTEND_IMAGE='registry.gitlab.com/bizzflow-etl/flowui/frontend:0.1.0'
FLOW_UI_BACKEND_IMAGE='registry.gitlab.com/bizzflow-etl/flowui/backend:0.1.1'
```

You can do that by running:

```bash
echo "FLOW_UI_FRONTEND_IMAGE='registry.gitlab.com/bizzflow-etl/flowui/frontend:0.1.0'" >> ~/project.env
echo "FLOW_UI_BACKEND_IMAGE='registry.gitlab.com/bizzflow-etl/flowui/backend:0.1.1'" >> ~/project.env
```

Place `docker-compose.yml` file to user's home dir (`/home/centos` by default) and run it:

_This uses `master` revision._ To use different revision, find it in git and copy the correct file version as
needed.

```console
curl -O -L https://gitlab.com/bizzflow-etl/flowui/backend/-/raw/master/docker-compose.yml
docker-compose -p bizzflow --env-file project.env up -d flow-ui
```
