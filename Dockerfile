FROM registry.gitlab.com/bizzflow-etl/toolkit:2.7.1
USER root

WORKDIR /app

ADD . ./

RUN poetry install --all-extras


USER bizzflow

