"""Background tasks"""

import asyncio
import json
from enum import Enum
from typing import Any, Callable, Dict, Iterable, NamedTuple, Optional, List

from celery.utils.log import get_task_logger
from celery.result import AsyncResult

from flow import env, schemas
from flow.cache import Cache, cache_client
from flow.utils.config import current_config
from flow.worker import celery

logger = get_task_logger(__name__)


async def _list_kex_tables(toolkit_kex: schemas.ToolkitKex) -> List[Dict[str, Any]]:
    """Return List of tables within a kex"""
    return [schemas.Table.create(table).dict() for table in await current_config.get_storage_manager().list_tables(toolkit_kex)]


@celery.task(name="refresh_storage")
def refresh_storage(root: bool = False, kex: Optional[str] = None):
    asyncio.run(async_refresh_storage(root, kex))


async def async_refresh_storage(root: bool = False, kex: Optional[str] = None):
    """Refresh storage tree (or a specified kex only)"""
    logger.info("Refreshing storage kexes and tables")
    cache = Cache()
    # Only load existing tree if refreshing whole tree
    tree = {} if not any((root, kex)) else (cache.get("storage_tree") or {})
    # If kex is set, only refresh specified kex, otherwise refresh all kexes
    kexes = [schemas.ToolkitKex(kex)] if kex else await current_config.get_storage_manager().list_kexes()
    for toolkit_kex in kexes:
        if root and toolkit_kex.kex in tree:
            logger.info("Skipping tables in kex %s because only root was supposed to be refreshed", toolkit_kex.kex)
            continue
        tree[toolkit_kex.kex] = await _list_kex_tables(toolkit_kex)
    # Remove any remnant from past cache
    if root and not kex:
        for extra_kex in set(tree.keys()) - set((k.kex) for k in kexes):
            logger.info("Kex %s was removed since last refresh", extra_kex)
            tree.pop(extra_kex)
    cache.set("storage_tree", tree, expires=None)
    logger.info("Storage tree was stored in the cache")


@celery.task(name="copy_table")
def copy_table(source: str, destination: str):
    asyncio.run(async_copy_table(source, destination))


async def async_copy_table(source: str, destination: str):
    """Copy table in background"""
    logger.info("Creating a copy of table '%s' into '%s'", source, destination)
    source_table = schemas.ToolkitTable.table_from_str(source)
    destination_table = schemas.ToolkitTable.table_from_str(destination.lower())
    if not (destination_table.kex.kex.startswith("out_") or destination_table.kex.kex.startswith("in_")):
        raise ValueError("Destination stage for copy operations must be either in_ or out_")
    if not destination_table.kex in await current_config.get_storage_manager().list_kexes():
        await current_config.get_storage_manager().create_kex(destination_table.kex)
    await current_config.get_storage_manager().copy_table(source_table, destination_table)
    logger.info("Table copy was created successfully")


@celery.task(name="failing_task")
def failing_task():
    raise ValueError("This was always meant to happen")


class TaskMeta(NamedTuple):
    task: Callable
    multi: Optional[bool] = False


tasks: Dict[str, TaskMeta] = {
    "refresh-storage": TaskMeta(refresh_storage, False),
    "failing-task": TaskMeta(failing_task, True),
    "table-copy": TaskMeta(copy_table, False),
}


class TaskEnum(str, Enum):
    REFRESH_STORAGE = "refresh-storage"
    FAILING_TASK = "failing-task"
    TABLE_COPY = "table-copy"

    @property
    def task(self) -> Callable:
        """Get Celery's task's callable"""
        if self in tasks.keys():
            return tasks[self].task
        raise ValueError(f"No task named {self} exists")

    @property
    def multi(self) -> bool:
        if self in tasks.keys():
            return bool(tasks[self].multi)
        raise ValueError(f"No task named {self} exists")


def get_task_queue() -> Iterable[schemas.Task]:
    """Get tasks from inspector (does not contain finished tasks)"""
    inspect = celery.control.inspect([env.CELERY_HOSTNAME])
    # For some reason, the inspect method returns None even if it should return dict, so in that case, we use empty dict
    scheduled = inspect.scheduled() or {}
    active = inspect.active() or {}
    reserved = inspect.reserved() or {}

    yield from (
        schemas.Task.from_queue(info)
        for info in [
            *scheduled.get(env.CELERY_HOSTNAME, []),
            *active.get(env.CELERY_HOSTNAME, []),
            *reserved.get(env.CELERY_HOSTNAME, []),
        ]
    )


def get_task_cache() -> Iterable[schemas.Task]:
    """Return all tasks from redis cache"""
    for key in cache_client.keys("celery-task-meta*"):
        content = cache_client.get(key)
        if content is None:
            continue
        yield schemas.Task(**json.loads(content))


def task_is_running(task) -> bool:
    """Returns whether an instance of the task is already running"""
    queue = get_task_queue()
    return task.name in (entry.name for entry in queue)


def schedule_once(task, *args, **kwargs) -> Optional[AsyncResult]:
    """Schedules task {task} to be run by celery worker if it is not scheduled / running yet.
    Returns True if the task run was created, returns False if the task run already existed
    """
    if task_is_running(task):
        return None
    return task.delay(*args, **kwargs)
