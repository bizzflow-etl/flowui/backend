"""Env and configuration
"""

import os
import logging

from dotenv import load_dotenv

logger = logging.getLogger(__name__)

load_dotenv()

REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379")
CELERY_HOSTNAME = os.getenv("CELERY_HOSTNAME", "celery@flowui")
ALLOW_DEV_USER = os.getenv("ALLOW_DEV_USER", "0") == "1"

if ALLOW_DEV_USER:
    logger.warning("Dev user is allowed, this should never be used in production environment")
