"""Schemas and dataclasses"""
from dataclasses import asdict
import re
from enum import Enum

# pylint: disable=too-few-public-methods
from datetime import datetime
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from toolkit.base.kex import Kex as ToolkitKex
from toolkit.base.table import Table as ToolkitTable
from toolkit.base.table import TableDetails as ToolkitTableDetails
from toolkit.base.metadata import ObjectMetadata
from typing import Any, Dict, List, NamedTuple, Optional, Union


class PropertyBaseModel(BaseModel):
    """
    Workaround for serializing properties with pydantic until
    https://github.com/samuelcolvin/pydantic/issues/935
    is solved
    """

    @classmethod
    def get_properties(cls):
        """Get list of custom properties"""
        return [
            prop
            for prop in dir(cls)
            if isinstance(getattr(cls, prop), property) and prop not in ("__values__", "fields")
        ]

    def dict(
        self,
        *,
        include: Union["AbstractSetIntStr", "MappingIntStrAny"] = None,  # type: ignore
        exclude: Union["AbstractSetIntStr", "MappingIntStrAny"] = None,  # type: ignore
        by_alias: bool = False,
        skip_defaults: bool = None,
        exclude_unset: bool = False,
        exclude_defaults: bool = False,
        exclude_none: bool = False,
    ) -> "DictStrAny":  # type: ignore
        """Overridden method in order to export properties along with fields"""
        attribs = super().dict(
            include=include,
            exclude=exclude,
            by_alias=by_alias,
            skip_defaults=skip_defaults,
            exclude_unset=exclude_unset,
            exclude_defaults=exclude_defaults,
            exclude_none=exclude_none,
        )
        props = self.get_properties()
        # Include and exclude properties
        if include:
            props = [prop for prop in props if prop in include]
        if exclude:
            props = [prop for prop in props if prop not in exclude]

        # Update the attribute dict with the properties
        if props:
            attribs.update({prop: getattr(self, prop) for prop in props})

        return attribs


class Kex(BaseModel):
    """Kex representation"""

    kex: str
    project: Optional[str] = None

    @staticmethod
    def create(kex: Union[ToolkitKex, str]) -> "Kex":
        """Create object from supported source"""
        if isinstance(kex, ToolkitKex):
            return Kex(project=kex.project, kex=kex.kex)
        parts = kex.split(".")
        if len(parts) == 2:
            return Kex(project=parts[0], kex=parts[1])
        return Kex(project=None, kex=parts[0])


class Table(BaseModel):
    """Table representation"""

    table: str
    kex: Kex

    @staticmethod
    def create(table: Union[ToolkitTable, str]) -> "Table":
        """Create object from supported source"""
        if isinstance(table, ToolkitTable):
            return Table(table=table.table, kex=Kex.create(table.kex))
        parts = table.split(".")
        return Table(table=parts[len(parts) - 1], kex=Kex.create(".".join(parts[: len(parts) - 1])))

    @property
    def as_str(self) -> str:
        """Return table as str (kex.table)"""
        return f"{self.kex.kex}.{self.table}"


class TableColumn(BaseModel):
    """Table column"""

    name: str
    type: str
    mode: Optional[str] = None

    @staticmethod
    def create(schema) -> "TableColumn":
        return TableColumn(name=schema.name, mode=schema.mode, type=schema.field_type)


class TableDetails(BaseModel):
    """Table details"""

    table: Table
    created: Optional[datetime] = None
    description: Optional[str] = None
    location: Optional[str] = None
    modified: Optional[datetime] = None
    size: Optional[int] = None
    size_readable: Optional[str] = None
    num_rows: Optional[int] = None
    path: Optional[str] = None
    fields: List[TableColumn]

    @staticmethod
    def create(table: ToolkitTable, details: ToolkitTableDetails) -> "TableDetails":
        return TableDetails(
            **details.to_dict(),
            fields=[TableColumn.create(field) for field in details.schema],
            table=Table.create(table),
        )


class TransformationType(str, Enum):
    """Transformation type"""

    SQL = "sql"
    DOCKER = "docker"


class Transformation(BaseModel):
    """Transformation representation"""

    id: str
    source: str
    type: TransformationType
    input_kexes: List[Kex]
    input_tables: List[Table]
    output: Kex
    files: List[str]

    @staticmethod
    def create(config: Dict[str, Any], files: Optional[List[str]] = None) -> "Transformation":
        """Create object from supported source"""
        files = files or []
        input_kexes = config.get("input_kexes", config.get("in_kex", []))
        input_tables = config.get("input_tables", config.get("in_tables", []))
        if isinstance(input_kexes, str):
            input_kexes = [input_kexes]
        return Transformation(
            id=config["id"],
            source=config["source"],
            type=config["type"],
            input_kexes=[Kex.create(k) for k in input_kexes],
            input_tables=[Table.create(t) for t in input_tables],
            output=Kex.create(config["out_kex"]),
            files=files,
        )


class CredentialsType(str, Enum):
    CREDENTIALS = "credentials"
    SANDBOX = "sandbox"
    DATAMART = "datamart"
    OTHER = "other"
    STORAGE = "storage"
    SHARE = "share"


SPECIAL_CREDENTIALS: Dict[str, CredentialsType] = {
    r"^google_application_credentials$": CredentialsType.STORAGE,
    r"^azure-sql.*$": CredentialsType.STORAGE,
    r"^snowflake-.*$": CredentialsType.STORAGE,
    r"^postgresql-.*$": CredentialsType.STORAGE,
    r"^dev[\-_].*$": CredentialsType.SANDBOX,
    r"^dm_.*$": CredentialsType.DATAMART,
    r"^sh_.*$": CredentialsType.SHARE,
}


class Credentials(PropertyBaseModel):
    """Credentials representation"""

    id: str
    value: Optional[str] = None

    @property
    def type(self) -> CredentialsType:
        """Credentials type (inferred from name prefix)"""
        normid = self.id.lower()
        for regex, cred_type in SPECIAL_CREDENTIALS.items():
            if re.match(regex, normid):
                return cred_type
        return CredentialsType.CREDENTIALS

    @property
    def show(self) -> bool:
        """Returns whether this type of credentials may be shown"""
        if self.type in [CredentialsType.SANDBOX, CredentialsType.DATAMART]:
            return True
        return False


class TaskState(str, Enum):
    """Task State from celery result backend"""

    #: Task state is unknown (assumed pending since you know the id).
    PENDING = "PENDING"
    #: Task was received by a worker (only used in events).
    RECEIVED = "RECEIVED"
    #: Task was started by a worker (:setting:`task_track_started`).
    STARTED = "STARTED"
    #: Task succeeded
    SUCCESS = "SUCCESS"
    #: Task failed
    FAILURE = "FAILURE"
    #: Task was revoked.
    REVOKED = "REVOKED"
    #: Task was rejected (only used in events).
    REJECTED = "REJECTED"
    #: Task is waiting for retry.
    RETRY = "RETRY"
    IGNORED = "IGNORED"


class Task(BaseModel):
    """Celery Task"""

    status: TaskState
    task_id: str
    name: str
    result: Optional[Any] = None
    date_done: Optional[datetime] = None
    date_start: Optional[datetime] = None
    worker: Optional[str] = None
    queue: Optional[str] = None
    retries: Optional[int] = None
    args: Optional[List[Any]] = None
    kwargs: Optional[Dict[Any, Any]] = None

    @staticmethod
    def from_queue(info: Dict[str, Any]) -> "Task":
        return Task(
            status="PENDING",
            result=None,
            task_id=info["id"],
            name=info["name"],
            date_start=info["time_start"],
            worker=info["hostname"],
        )


class User(PropertyBaseModel):
    """User"""

    id: int
    name: str
    email: str
    picture: Optional[str] = None
    provider: Optional[str] = "airflow"

    @property
    def sandbox_name(self) -> str:
        """Sandbox name for the user"""
        return "dev_{}".format("".join([part[:3] for part in self.email.split("@")[0].split(".")]))


class SandboxLoadConfig(BaseModel):
    """Sandbox Load Conig"""

    clean: bool = False
    transformation_id: Optional[str] = None
    additional_tables: Optional[List[Table]] = None
    run_until: Optional[str] = None
    run: bool = False


class SandboxRunConfig(NamedTuple):
    """Sandbox run configuration schema"""

    sandbox_user_email: str
    additional_kexes: List[str]
    additional_tables: List[str]
    run_options: Dict
    clean_sandbox: bool = False
    load_transformation: bool = True
    dry_run: bool = False
    transformation_id: Optional[str] = None


class StorageObjectMetadata(BaseModel):
    """Storage object metadata"""

    name: str
    description: Optional[str] = None
    component: Optional[str] = None
    user: Optional[str] = None
    created: datetime

    @staticmethod
    def from_toolkit(metadata: ObjectMetadata) -> "StorageObjectMetadata":
        """From toolkit object"""
        return StorageObjectMetadata(**asdict(metadata))

    @property
    def object_metadata(self) -> ObjectMetadata:
        """Get toolkit object"""
        return ObjectMetadata(**self.dict())
