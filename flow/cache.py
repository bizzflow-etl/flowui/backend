import json
from typing import Dict, Optional, Union
import redis
from flow import env

cache_client = redis.Redis.from_url(env.REDIS_URL)


class Cache:
    """Primitive class to store and retrieve values from redis"""

    def __init__(self, client: Optional[redis.Redis] = None):
        self.client = client or cache_client

    def set(self, key: str, data: Union[str, Dict], expires: Optional[int] = 1800):
        """Store {data} in cache with {key}. The data expires in {expires} seconds"""
        if isinstance(data, Dict):
            strdata = json.dumps(data)
        else:
            strdata = data
        self.client.set(key, strdata, ex=expires)

    def delete(self, *keys: str):
        """Delete existing {key} from redis cache"""
        self.client.delete(*keys)

    def get(self, key: str) -> Optional[Dict]:
        """Get data previously stored with {key} in cache or None if nonexistent or expired"""
        strdata = self.client.get(key)
        if strdata is None:
            return None
        return json.loads(strdata)
