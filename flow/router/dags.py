"""Router for accessing DAGs"""

from typing import List
from fastapi import APIRouter, Depends

from flow import schemas, deps
from flow.utils import airflow

dagrouter = APIRouter(prefix="/dags", tags=["DAG", "Airflow"])


@dagrouter.get("/", response_model=List[str])
async def list_dags(_user: schemas.User = Depends(deps.get_session_user)):
    """List dags"""
    bag = airflow.get_dagbag()
    return [did for did in bag.dag_ids]


@dagrouter.get("/{dag_id}")
async def get_dag(dag_id: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Get dag"""
    airflow.get_dag(dag_id)
    return dag_id
