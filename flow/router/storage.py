"""Storage API endpoints
"""
# pylint: disable=fixme
from typing import Any, Dict, List, Union
from toolkit.base.kex import Kex as ToolkitKex
from toolkit.base.table import Table as ToolkitTable
from fastapi import APIRouter, Depends

from flow import schemas, deps, exceptions
from flow.cache import Cache
from flow import background_tasks
from flow.utils.helpers import is_kex_public, is_kex_visible
from flow.utils.config import current_config

storage = APIRouter(prefix="/storage", tags=["storage"])


def kex_exists(kex: str) -> bool:
    """Returns whether kex exists in current cached tree"""
    cache = Cache()
    tree = cache.get("storage_tree") or {}
    return kex in tree


def table_exists(kex: str, table: str) -> bool:
    """Returns whether table exists in current cached tree"""
    cache = Cache()
    tree = cache.get("storage_tree") or {}
    if not kex in tree:
        return False
    return table in (item["table"] for item in tree[kex])


# TODO: Return basic storage info (backend, database name, etc.)
@storage.get("/")
async def get_storage_info(_user: schemas.User = Depends(deps.get_session_user)):
    """Get storage info and metadata"""
    return {"storage": "This endpoint is useless"}


@storage.get("/kex", response_model=List[schemas.Kex])
async def list_kexes(_user: schemas.User = Depends(deps.get_session_user)):
    """List kexes from storage"""
    return sorted(
        [schemas.Kex.create(kex) for kex in await current_config.get_storage_manager().list_kexes()], key=lambda k: k.kex
    )


@storage.get("/kex/{kex_name}", response_model=schemas.Kex)
async def get_kex_details(kex_name: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Get kex details"""
    cache = Cache()
    tree = cache.get("storage_tree") or {}
    if not kex_name in tree.keys():
        raise exceptions.KexNotFound(kex_name)
    return schemas.Kex(kex=kex_name, project=None)


@storage.get("/kex/{kex_name}/tables", response_model=schemas.Table)
async def list_tables(kex_name: str, _user: schemas.User = Depends(deps.get_session_user)):
    """List tables within specified kex"""
    toolkit_kex = ToolkitKex(kex_name)
    return sorted(
        [schemas.Table.create(table) for table in await current_config.get_storage_manager().list_tables(toolkit_kex)],
        key=lambda t: t.table,
    )


@storage.get("/kex/{kex_name}/tables/{table}", response_model=schemas.TableDetails)
async def get_table_details(kex_name: str, table: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Get table details"""
    if not table_exists(kex_name, table):
        raise exceptions.NotFound(f"{kex_name}.{table}", "table")
    toolkit_kex = ToolkitKex(kex_name)
    toolkit_table = ToolkitTable(table, toolkit_kex)
    details = await current_config.get_storage_manager().get_table_details(toolkit_table)
    return schemas.TableDetails.create(toolkit_table, details)


async def _get_metadata(toolkit_table: ToolkitTable) -> Union[schemas.StorageObjectMetadata, Dict]:
    metadata = await current_config.get_storage_manager().get_table_metadata(toolkit_table)
    if metadata is None:
        return {}
    return schemas.StorageObjectMetadata.from_toolkit(metadata)


@storage.get("/kex/{kex_name}/tables/{table}/metadata", response_model=Union[schemas.StorageObjectMetadata, Dict])
async def get_table_metadata(kex_name: str, table: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Get table metadata"""
    toolkit_kex = ToolkitKex(kex_name)
    toolkit_table = ToolkitTable(kex=toolkit_kex, table_name=table)
    return await _get_metadata(toolkit_table)


@storage.post("/kex/{kex_name}/tables/{table}/metadata", response_model=Union[schemas.StorageObjectMetadata, Dict])
async def set_table_metadata(
    kex_name: str,
    table: str,
    data: schemas.StorageObjectMetadata,
    _user: schemas.User = Depends(deps.get_session_user),
):
    """Set table metadata"""
    toolkit_kex = ToolkitKex(kex_name)
    toolkit_table = ToolkitTable(kex=toolkit_kex, table_name=table)
    await current_config.get_storage_manager().set_table_metadata(toolkit_table, data.object_metadata)
    return await _get_metadata(toolkit_table)


@storage.delete("/kex/{kex_name}")
async def delete_kex(kex_name: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Delete kex"""
    toolkit_kex = ToolkitKex(kex_name)
    await current_config.get_storage_manager().delete_kex(toolkit_kex)


@storage.delete("/kex/{kex_name}/tables/{table}")
async def delete_table(kex_name: str, table: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Delete table from storage"""
    toolkit_kex = ToolkitKex(kex_name)
    toolkit_table = ToolkitTable(kex=toolkit_kex, table_name=table)
    await current_config.get_storage_manager().delete_table(toolkit_table)


@storage.post("/kex/{kex_name}/tables/{table}/truncate")
async def truncate_table(kex_name: str, table: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Delete table from storage"""
    toolkit_kex = ToolkitKex(kex_name)
    toolkit_table = ToolkitTable(kex=toolkit_kex, table_name=table)
    await current_config.get_storage_manager().truncate_table(toolkit_table)


@storage.get("/kex/{kex_name}/tables/{table}/preview", response_model=List[Dict[str, Any]])
async def get_table_preview(kex_name: str, table: str, _user: schemas.User = Depends(deps.get_session_user)):
    """Get table preview"""
    toolkit_kex = ToolkitKex(kex_name)
    toolkit_table = ToolkitTable(kex=toolkit_kex, table_name=table)
    preview = await current_config.get_storage_manager().preview(toolkit_table, 100)
    preview = preview.get("rows", [])
    return preview


@storage.get("/tree")
async def get_storage_tree(
    refresh: bool = False, public: bool = True, _user: schemas.User = Depends(deps.get_session_user)
):
    """Get storage tree from cache"""
    cache = Cache()
    tree = cache.get("storage_tree") or {}
    if not tree or refresh:
        background_tasks.schedule_once(background_tasks.refresh_storage)
    return {
        kex: sorted(tree[kex], key=lambda t: t["table"])
        for kex in sorted(tree.keys())
        if (public and is_kex_public(kex)) or (not public and is_kex_visible(kex))
    }
