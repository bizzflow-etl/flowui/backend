"""Sandbox endpoints"""

from fastapi import APIRouter, Depends
from flow import deps, schemas, exceptions
from flow.utils import airflow, transformations

sandbox = APIRouter(prefix="/sandbox", tags=["sandbox"])


@sandbox.put("/{sandbox_name}")
async def load_sandbox(
    sandbox_name: str, config: schemas.SandboxLoadConfig, user: schemas.User = Depends(deps.get_session_user)
):
    if sandbox_name != user.sandbox_name:
        raise exceptions.ActionNotAllowed(user, f"load sandbox {sandbox_name}")
    if config.transformation_id is None or not config.run:
        skip_files = []
    elif config.run_until is None:
        skip_files = []
    else:
        trans = transformations.get_transformation_by_id(config.transformation_id)
        try:
            last_file_index = trans.files.index(config.run_until)
        except ValueError as error:
            raise exceptions.NotFound(config.run_until, "Transformation file") from error
        skip_files = trans.files[last_file_index + 1 :]

    sbox_config = schemas.SandboxRunConfig(
        sandbox_user_email=user.email,
        additional_kexes=[],
        additional_tables=[table.as_str for table in config.additional_tables],
        run_options={"skip_files": skip_files},
        clean_sandbox=config.clean,
        load_transformation=config.transformation_id is not None,
        dry_run=config.run,
        transformation_id=config.transformation_id,
    )
    airflow.run_dag(dag="80_Sandbox", user=user, config=sbox_config._asdict())
