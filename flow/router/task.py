"""Background tasks endpoint"""

import sys
import asyncio
import logging
from typing import Any, Dict, List, Optional
from datetime import datetime
from fastapi import APIRouter, Depends

from flow import background_tasks, exceptions, schemas, deps

logger = logging.getLogger(__name__)
task = APIRouter(prefix="/task", tags=["tasks"])


def task_sort(task_data: schemas.Task) -> str:
    if task_data.status == schemas.TaskState.PENDING:
        status_prefix = "A"
        timestamp = task_data.date_start.timestamp() if task_data.date_start else 0
        date_suffix = str(sys.maxsize - timestamp)
    else:
        status_prefix = "Z"
        timestamp = task_data.date_done.timestamp() if task_data.date_done else 0
        date_suffix = str(sys.maxsize - timestamp)
    return f"{status_prefix}{date_suffix}"


@task.get("/", response_model=List[schemas.Task])
async def list_tasks(_user: schemas.User = Depends(deps.get_session_user)):
    """Get list of running and finished tasks"""
    queue = [*background_tasks.get_task_cache(), *background_tasks.get_task_queue()]
    queue.sort(key=task_sort)
    return queue[:7]


@task.post("/{task_type}")
async def create_task_instance(
    task_type: background_tasks.TaskEnum,
    body: Dict[str, Any] = Depends(deps.get_raw_body),
    sync: Optional[int] = None,
    _user: schemas.User = Depends(deps.get_session_user),
):
    """Start specified task, if {sync} is specified it is maximal number of seconds
    to wait before returning. In this case, {status} in the response will be set to "Finished".
    If the task did not finish within specified number of seconds, it will
    continue running in the background worker and {status} will be "Scheduled".
    """
    if not task_type.multi and background_tasks.task_is_running(task_type.task):
        return {"status": f"Another instance of task {task_type} is already running"}
    try:
        result = task_type.task.delay(**body)
        if not sync:
            return {"status": "Scheduled"}
        start_time = datetime.utcnow()
        while (datetime.utcnow() - start_time).seconds < sync:
            if result.ready():
                return {"status": "Finished"}
            await asyncio.sleep(1)
        return {"status": "Scheduled"}
    except TypeError as error:
        logger.error(error, exc_info=True)
        raise exceptions.BadRequest(
            f"Make sure to provide all arguments to the task function ({str(error)})"
        ) from error
