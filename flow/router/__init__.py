"""Import sub-routers to make imports easier and more readable"""

from flow.router.storage import storage
from flow.router.project import project
from flow.router.vault import vault
from flow.router.task import task
from flow.router.auth import auth
from flow.router.sandbox import sandbox
from flow.router.dags import dagrouter
