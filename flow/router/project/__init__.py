"""Join all sub-routers to a single project router, makes importing easier"""

from fastapi import APIRouter

from flow.router.project.transformation import transformation

project = APIRouter(prefix="/project", tags=["project"])

project.include_router(transformation)
