"""Transformation API endpoints
"""

from typing import List

from fastapi import APIRouter, Depends

from flow import deps, schemas
from flow.utils import transformations

transformation = APIRouter(prefix="/transformation", tags=["transformation", "project"])


@transformation.get("/", response_model=List[schemas.Transformation])
async def list_transformations(_user: schemas.User = Depends(deps.get_session_user)):
    """List transformation configurations within project"""
    return list(transformations.iter_transformations())
