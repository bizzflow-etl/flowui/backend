"""
Authentication endpoints
"""

from typing import Optional
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse

from flow import schemas, deps, exceptions

auth = APIRouter(prefix="/auth", tags=["authentication"])


@auth.get("/", response_model=Optional[schemas.User])
async def get_user(user: schemas.User = Depends(deps.get_session_user)):
    return user.dict()


@auth.get("/logout")
async def get_user():
    content = {"message": "Logged out"}
    response = JSONResponse(content=content)
    response.delete_cookie("flowui_session")
    return response
