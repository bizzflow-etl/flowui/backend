"""Vault API endpoints"""

from typing import Dict, List
from fastapi import APIRouter, Depends

from flow import schemas, deps, exceptions
from flow.utils.config import current_config

vault = APIRouter(prefix="/vault", tags=["vault"])


def _credentials_exist(cred: schemas.Credentials) -> bool:
    """Returns True whether credentials' id already exists"""
    return cred.id in current_config.get_vault_manager().list_credentials()


@vault.get("/", response_model=List[schemas.Credentials])
async def list_credentials(_user: schemas.User = Depends(deps.get_session_user)):
    """List all credential keys (does not return credential secrets)"""
    credentials = current_config.get_vault_manager().list_credentials()
    return sorted(
        (schemas.Credentials(id=key) for key in credentials if not key.lower().startswith("tr_")),
        key=lambda cred: cred.id,
    )


@vault.get("/{key}", response_model=schemas.Credentials)
async def get_credentials(key: str, user: schemas.User = Depends(deps.get_session_user)):
    """Get a secret for single credential"""
    cred = schemas.Credentials(id=key, value=current_config.get_vault_manager().get_credentials(key))
    if cred.type == schemas.CredentialsType.SANDBOX and cred.id != user.sandbox_name:
        raise exceptions.ActionNotAllowed(user, f"display credentials for {cred.id}")
    if cred.type not in [schemas.CredentialsType.DATAMART,
                         schemas.CredentialsType.SANDBOX,
                         schemas.CredentialsType.SHARE
                         ]:
        cred.value = ""
    return cred


@vault.get("/{key}/type", response_model=Dict[str, str])
async def infer_credentials_type(key: str, _user: schemas.User = Depends(deps.get_session_user)) -> Dict[str, str]:
    """Get credentials type based on its id"""
    cred = schemas.Credentials(id=key)
    return {"type": cred.type}


@vault.post("/", response_model=schemas.Credentials)
async def save_credentials(cred: schemas.Credentials, user: schemas.User = Depends(deps.get_session_user)):
    """Save credentials"""
    if (
        cred.type
        in (
            schemas.CredentialsType.DATAMART,
            schemas.CredentialsType.SANDBOX,
            schemas.CredentialsType.SHARE,
            schemas.CredentialsType.OTHER,
        )
        and _credentials_exist(cred)
    ):
        raise exceptions.ActionNotAllowed(user, "overwrite special credentials")
    current_config.get_vault_manager().store_credentials(cred.id, cred.value)
    return cred
