from datetime import datetime, timedelta

from toolkit import current_config as bizzflow_current_config
from toolkit.dags.helpers.pools import PoolManager


class BizzflowConfig:
    def __init__(self):
        self._last_updated = datetime.now()
        self._config = bizzflow_current_config
        self._pool_manager = PoolManager()

    def __getattribute__(self, item):
        if item in ["_last_updated", "_config", "_pool_manager"]:
            return super().__getattribute__(item)
        if self._last_updated < datetime.now() - timedelta(minutes=1):
            self._config.refresh()
            # Refresh pools - this is a workaround until it will be fixed in toolkit
            # just open new session to get the latest pools
            self._pool_manager.open_session()
            self._last_updated = datetime.now()
        return getattr(self._config, item)


current_config = BizzflowConfig()
