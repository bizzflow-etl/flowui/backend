"""Basic helpers
"""

from typing import Optional, Union
from hashlib import md5

from flow import schemas

NONPUBLIC_PREFIXES = ["dev", "tr_", "tmp", "raw"]
NONPUBLIC_KEXES = ["information_schema", "public"]
PUBLIC_PREFIXES = ["dm_", "in_", "out_"]
PUBLIC_PREFIXES_NON_STRICT = ["raw_", "in_", "dm_", "out_"]

dev_admin = schemas.User(
    id=0,
    name="admin",
    email="bizzflow.admin@bizzflow.app",
    picture="/admin.png",
)


def is_kex_public(kex: Union[schemas.Kex, str]) -> bool:
    """
    Returns True if kex is suitable for viewing in storage tree
    """
    if isinstance(kex, schemas.Kex):
        kex = kex.kex
    for prefix in PUBLIC_PREFIXES:
        if kex.lower().startswith(prefix):
            return True
    return False


def is_kex_visible(kex: Union[schemas.Kex, str]) -> bool:
    """Returns Tre if kex is suitable for viewing in non-strict way in storage tree"""
    if isinstance(kex, schemas.Kex):
        kex = kex.kex
    for prefix in PUBLIC_PREFIXES_NON_STRICT:
        if kex.lower().startswith(prefix):
            return True
    return False


def get_gravatar(email: str, size: Optional[int] = 40) -> str:
    """Get gravatar url to avatar based on email"""
    email_md5 = md5(email.lower().encode("utf-8")).hexdigest()
    return f"https://gravatar.com/avatar/{email_md5}?s={size}"
