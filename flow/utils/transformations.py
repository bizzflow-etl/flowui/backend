"""Transformation helpers"""

import os
from glob import glob
from flow import schemas, exceptions
from typing import Iterable


from flow.utils.config import current_config


def iter_transformations() -> Iterable[schemas.Transformation]:
    """Iterate through transformation objects"""
    for tid in current_config.get_transformations_ids():
        trans = current_config.loader.transformation_loader.get_component_config(tid)
        sources = os.path.join(current_config.project_path, "transformations", trans["source"], "*.sql")
        files = [os.path.basename(fname) for fname in glob(sources)]
        files.sort()
        yield schemas.Transformation.create(trans, files=files)


def get_transformation_by_id(trid: str) -> schemas.Transformation:
    """Get transformation by its id"""
    for trans in iter_transformations():
        if trans.id == trid:
            return trans
    raise exceptions.NotFound(trid, "Transformation")
