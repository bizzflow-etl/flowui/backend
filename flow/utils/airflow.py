"""Airflow utils
"""

import logging
import re
from types import TracebackType
import traceback
from datetime import datetime
from typing import Dict, Optional, Type, Union

from airflow.configuration import conf

from airflow.models import DAG, DagBag
from airflow.settings import Session as _AirflowSession
from airflow.utils.state import State as DAGState

from flow import exceptions, schemas

logger = logging.getLogger(__name__)


class AirflowSession:
    """Context manager to make sure Airflow session is closed"""

    def __init__(self):
        self.session = None

    def __enter__(self):
        if _AirflowSession is not None:
            logger.info("Opening a new Airflow metadata DB session")
            self.session = _AirflowSession()
            return self.session
        raise ValueError("Could not get Airflow session")

    def __exit__(
            self,
            exc_type: Optional[Type[BaseException]],
            exc_value: Optional[BaseException],
            exc_traceback: Optional[TracebackType],
    ) -> Optional[bool]:
        if self.session is None:
            raise ValueError("Airflow Session was not properly opened. Use AirflowSession as a context manager.")
        if exc_type is not None:
            logger.error(traceback.format_exception(exc_type, exc_value, exc_traceback))
            logger.warning("Rolling Airflow metadata session back due to an error")
            self.session.rollback()
            return
        logger.info("Closing Airflow metadata session")
        self.session.commit()
        self.session.close()


def get_secret_key() -> str:
    return conf.get("webserver", "SECRET_KEY")


def get_dagbag() -> DagBag:
    """Get dagbag"""
    return DagBag(include_examples=False, read_dags_from_db=True)


def get_dag(dag_id: str) -> DAG:
    """Get dag by id"""
    bag = get_dagbag()
    dag = bag.get_dag(dag_id)
    if dag is None:
        raise exceptions.NotFound(dag_id, "Airflow DAG")
    return dag


def generate_run_id(runtype: str = "flowUI", user: Optional[schemas.User] = None) -> str:
    """Generate str run id for Airflow DAGRun"""
    datestr = datetime.now().isoformat()
    username = user.name if user else "auto"
    run_id = f"{username}_{runtype}__{datestr}"
    run_id = re.sub(r'[^A-Za-z0-9_.~:+-]+', '-', run_id)
    return run_id


def run_dag(dag: Union[DAG, str], user: Optional[schemas.User] = None, external: bool = False, config: Dict = None):
    """Run specified DAG as a specified user"""
    if isinstance(dag, str):
        dag = get_dag(dag)
    run_id = generate_run_id(user=user)
    dag.create_dagrun(
        run_id=run_id,
        state=DAGState.NONE,
        external_trigger=external,
        conf=config,
    )
