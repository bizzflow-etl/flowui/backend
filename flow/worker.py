from celery import Celery

from flow import env

celery = Celery(
    __name__, broker=env.REDIS_URL, backend=env.REDIS_URL, include=["flow.background_tasks"], result_extended=True
)
celery.conf.update(TASK_SERIALIZER="json", RESULT_SERIALIZER="json", ACCEPT_CONTENT=["json"])
