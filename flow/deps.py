"""Dependencies for FastAPI
"""

import json
from typing import Any, Dict, Optional

import jwt
from fastapi import Cookie, Request
from flow import schemas, exceptions
from flow.utils import airflow, helpers
from flow.env import ALLOW_DEV_USER
from flow.utils.helpers import get_gravatar


async def get_session_user(
    request: Request,
    flowui_session: Optional[str] = Cookie(None),
    dev_user: Optional[bool] = Cookie(False),
) -> schemas.User:
    """General auth dependency to verify we have a user information"""
    # print(request.headers)
    if dev_user and ALLOW_DEV_USER:
        return helpers.dev_admin
    if flowui_session is not None:
        try:
            user_data = jwt.decode(flowui_session, airflow.get_secret_key(), algorithms=["HS256"])
        except jwt.exceptions.InvalidTokenError:
            raise exceptions.InvalidToken()
        return schemas.User(id=user_data["id"], email=user_data["email"], name=user_data["name"], picture=get_gravatar(user_data["email"]))
    raise exceptions.NotAuthenticated()


async def get_raw_body(request: Request) -> Dict[str, Any]:
    """Get raw body from request"""
    try:
        return await request.json()
    except json.decoder.JSONDecodeError:
        # When body is None or an invalid JSON, just ignore it
        return {}
