"""Exceptions
"""

from flow import schemas
from typing import Union
from fastapi import HTTPException
from fastapi import status


class NotAuthenticated(HTTPException):
    def __init__(self):
        super().__init__(status.HTTP_401_UNAUTHORIZED, "You must login first", headers={"www-authenticate": "/"})


class InvalidToken(HTTPException):
    def __init__(self):
        super().__init__(
            status.HTTP_401_UNAUTHORIZED, "Provided session token is invalid", headers={"www-authenticate": "/"}
        )


class KexNotFound(HTTPException):
    def __init__(self, kex: Union[str, schemas.Kex, schemas.ToolkitKex]):
        if isinstance(kex, (schemas.Kex, schemas.ToolkitKex)):
            kex = kex.kex
        super().__init__(status.HTTP_404_NOT_FOUND, f"Specified kex '{kex}' was not found")


class ActionNotAllowed(HTTPException):
    def __init__(self, user: schemas.User, action: str):
        super().__init__(status.HTTP_403_FORBIDDEN, detail=f"{user.name} is not allowed to perform action '{action}'")


class NotFound(HTTPException):
    def __init__(self, resource: str, _type: str = "resource"):
        super().__init__(status.HTTP_404_NOT_FOUND, detail=f"{_type} '{resource}' was not found")


class BadRequest(HTTPException):
    def __init__(self, help: str):
        super().__init__(status.HTTP_400_BAD_REQUEST, detail=f"Your request could not be processed. {help}")
