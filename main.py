"""Flow UI API Backend"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from flow import router

app = FastAPI(title="FlowUI API")

cors_origins = ["http://localhost:8080", "http://localhost:8081"]
cors_origins_regex = r"https:\/\/.*\.bizzflow\.app"  # pylint: disable=invalid-name

app.add_middleware(
    CORSMiddleware,
    allow_origins=cors_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    allow_origin_regex=cors_origins_regex,
)


@app.get("/health")
async def health_check():
    """Primitive health check"""
    from toolkit import current_config  # pylint: disable=import-outside-toplevel

    current_config.get_transformations_ids()
    return {"status": "healthy"}


app.include_router(router.storage)
app.include_router(router.project)
app.include_router(router.vault)
app.include_router(router.task)
app.include_router(router.auth)
app.include_router(router.sandbox)
app.include_router(router.dagrouter)
